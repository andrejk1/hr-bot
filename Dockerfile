FROM maven:3.5-jdk-8-alpine as build
WORKDIR /app
COPY ./ /app
RUN mvn install -DskipTests=true

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/target/TG_Bot_v4-1.0-SNAPSHOT.jar /app

CMD ["java", "-jar", "-Dspring.profiles.active=${ACTIVE_PROFILE}", "/app/TG_Bot_v4-1.0-SNAPSHOT.jar"]
